#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <cstdint>
#include <cassert>
#include <random>
#include <memory>
#include <cassert>

const bool DEBUG = false;

using namespace std;

// == OutBittStream ==

class BitStream
{
public:

  // common
  
  BitStream();
  size_t size_u64() const;
  size_t size() const;
  
  // for output
  
  void push(const uint64_t data, const uint64_t length);
  void write(ofstream &fout);
  void trim();
  bool trimed() const;
  
  // for input
  // 破壊的にReadすることに注意
  
  uint64_t get(const int64_t length);
  
  void read(ifstream &fin, const size_t sz);

  vector<uint64_t> data_array;
  
  // for output
private:

  uint64_t data_single;
  uint64_t length_single;

  // for input
  
  uint64_t rest_bit;
  int64_t index;
};

// {{{

BitStream::BitStream()
{
  data_single = 0;
  length_single = 0;
  rest_bit = 64;
  index = 0;
}

bool BitStream::trimed() const
{
  return length_single == 0;
}

// uint64_t 単位でのサイズ
size_t BitStream::size_u64() const
{
  return data_array.size() + (length_single > 0 ? 1 : 0);
}

size_t BitStream::size() const
{
  return data_array.size() * 64ull + length_single;
}

void BitStream::push(const uint64_t data, const uint64_t length)
{
  assert(length <= 64);
  
  if (length_single + length >= 64)
  {
	const uint64_t front_length = 64 - length_single;
	const uint64_t back_length  = length_single + length - 64;
	
	const uint64_t front_data = data >> back_length;
	const uint64_t back_data = data & ((1 << back_length) - 1);
	
	data_single <<= front_length;
	data_single += front_data;
	data_array.push_back(data_single);
	
	data_single = back_data;
	length_single = back_length;
  }
  else
  {
	data_single <<= length;
	data_single += data;
	length_single += length;
  }
}

void BitStream::trim()
{
  if (length_single > 0)
  {
	data_single <<= (64ull - length_single);
	length_single = 0;
	data_array.push_back(data_single);
  }
}

void BitStream::write(ofstream &fout)
{
  trim();
  char *ptr = reinterpret_cast<char *>(data_array.data());
  fout.write(ptr, data_array.size() * sizeof(uint64_t));
}

uint64_t BitStream::get(const int64_t length)
{
  assert(0 < length && length <= 64);
  
  if (length > rest_bit)
  {
	const uint64_t front_data = data_array[index] >> (64 - rest_bit);
	const int back_length = length - rest_bit;
	index++;
	rest_bit = 64;
	
	const uint64_t back_data = data_array[index] >> (64 - back_length);
	rest_bit -= back_length;
	data_array[index] <<= back_length;
	
	return (front_data << back_length) + back_data;
  }
  else
  {
	const uint64_t ans = data_array[index] >> (64 - length);
	rest_bit -= length;
	data_array[index] <<= length;
	
	return ans;
  }
}

void BitStream::read(ifstream &fin, const size_t sz)
{
  data_array.resize(sz);
  char *ptr = reinterpret_cast<char *>(data_array.data());
  fin.read(ptr, sz * sizeof(uint64_t));
}

// }}}


// == BypassEncoder ==

class BypassEncoder
{
public:

  BypassEncoder(BitStream *ostream);
  ~BypassEncoder();
  
  void encode_unary(const bool sign);  
  void encode_pixel(const int64_t val);
  
  template<typename T>
  void encode_bypass(const T data)
  {
	ostream->push(data, sizeof(T) * 8);
  }

private:
  BitStream *ostream;
};

// {{{

BypassEncoder::~BypassEncoder(){}

BypassEncoder::BypassEncoder(BitStream *os)
{
  ostream = os;
}

void BypassEncoder::encode_unary(const bool sign)
{
  ostream->push(sign ? 1 : 0, 1);
}

void BypassEncoder::encode_pixel(const int64_t val)
{
  assert(0 <= val && val < (1 << 16));
  
  ostream->push(val, 16);
}


// }}}

// == BypassDecoder ==

class BypassDecoder
{
public:

  BypassDecoder(BitStream *is);
  ~BypassDecoder();
  
  bool decode_unary();
  int64_t decode_pixel();

  template<typename T>
  T decode_bypass()
  {
	return istream->get(sizeof(T) * 8);
  }

private:
  BitStream *istream;
};

// {{{

BypassDecoder::~BypassDecoder(){}

BypassDecoder::BypassDecoder(BitStream *is)
{
  istream = is;
}

bool BypassDecoder::decode_unary()
{
  const uint64_t val = istream->get(1);
  return (val == 1);
}

int64_t BypassDecoder::decode_pixel()
{
  return istream->get(16);
}

// }}}

// == RangeCoder ==

class RangeCoder
{
public:
  void encode(BitStream &istream, BitStream &ostream);
  void decode(BitStream &istream, BitStream &ostream);
  
private:
  double count_zero_rate(const BitStream &stream);

  const uint64_t FULL = (1ull << 32);
  const uint64_t THIRD_QUAT = (3ull << 30);
  const uint64_t HALF = (1ull << 31);
  const uint64_t QUAT = (1ull << 30);
  const uint64_t MASK_32 = 0xffffffff;
};

double RangeCoder::count_zero_rate(const BitStream &stream)
{
  int64_t ans = 0;
  for (uint64_t v : stream.data_array)
  {
	ans += __builtin_popcountll(v);
  }
  return (stream.size() - ans) * 1.0 / stream.size();
}

void RangeCoder::encode(BitStream &istream, BitStream &ostream)
{
  assert(istream.trimed());

  const uint64_t rate0 = FULL * count_zero_rate(istream);
  const uint64_t rate1 = FULL - rate0;
  if (DEBUG) cerr << "enc: " << (rate0 * 1.0 / FULL) << endl;
  
  const int raw_size = istream.size();
  
  ostream.push(rate0, 32);
  ostream.push(istream.size(), 32);
  
  uint64_t lower = 0;
  uint64_t range = FULL;
  
  uint64_t code = 0;
  uint64_t count = 0;
  
  for (int i = 0; i < raw_size; i++)
  {
	if (DEBUG) cerr << "==================== " << endl;
	const uint64_t val = istream.get(1);
	const uint64_t range0 = (range * rate0) >> 32;
	
	if (val == 1)
	{
	  range = (range * rate1) >> 32;
	  lower += range0;
	}
	else
	{
	  range = range0;
	}

	assert(range <= FULL);
	if (DEBUG) cerr << hex << "mid enc: [" << lower << ", " << (lower + range) << ")" << dec << endl;
	assert(lower + range <= FULL);
	
	while (range < HALF)
	{
	  if (lower >= HALF)
	  {
		lower = (lower - HALF) << 1;
		range <<= 1;
		ostream.push(1, 1);
		if (DEBUG) cerr << 1 << " ";
		
		code <<= 1;
		code += 1;
		count++;
	  }
	  else if(lower + range < HALF)
	  {
		range <<= 1;
		lower <<= 1;
		ostream.push(0, 1);
		if (DEBUG) cerr << 0 << " ";

		code <<= 1;
		count++;
	  }
	  // else if(QUAT <= lower && lower + range < THIRD_QUAT)
	  // {
	  // 	lower = (lower - QUAT) << 1;
	  // 	range <<= 1;
	  // }
	  else
	  {
		break;
	  }
	  if (DEBUG) if (count == 32) cerr << "hoge " << hex << code << dec << endl;
	}
	if (DEBUG) cerr << hex << "enc: [" << lower << ", " << (lower + range) << ")" << dec << endl;
  }
  ostream.push(lower + range / 2, 32);
}

void RangeCoder::decode(BitStream &istream, BitStream &ostream)
{
  const uint64_t rate0 = istream.get(32);
  const uint64_t rate1 = FULL - rate0;
  const int64_t ostream_size = istream.get(32);

  if (DEBUG) cerr << "dec: " << (rate0 * 1.0 / FULL) << endl;
  
  uint64_t lower = 0;
  uint64_t range = FULL;

  const int rc_size = istream.size() - 64;
  assert(rc_size > 0);
  
  uint64_t code = istream.get(min<int>(32, rc_size));
  code <<= (32 - min<int>(32, rc_size));
  
  if (DEBUG)
  {
	cerr << (code * 1.0 / FULL) << endl;
	cerr << hex << code << dec << endl;
	
	for (int i = 0; i < 32; i++)
	{
	  cerr << ((code >> (31 - i)) & 1) << " ";
	}
	cerr << endl;
  }
  
  for (int i = 0; i < ostream_size; i++)
  {
	const uint64_t range0 = (range * rate0) >> 32;
	
	if (DEBUG)
	{
	  cerr << "==================== " << endl;
	  assert(0 <= range0 && range0 < FULL);
	  
	  cerr << hex << "dec : " << lower << " < " << code << " < " << (lower + range) << dec << endl;
	  cerr << hex << "dec : [" << (lower + range0) << ", " << (lower + range) << ")" << dec << endl;
	  cerr << hex << "dec : [" << lower << ", " << (lower + range0) << ")" << dec << endl;
	  assert(lower <= code && code <= lower + range);
	}
	
	if (lower + range0 <= code && code < lower + range) // select 1
	{
	  range = (range * rate1) >> 32;
	  lower += range0;
	  ostream.push(1, 1);
	}
	else if(lower <= code && code < lower + range0) // select 0
	{
	  range = range0;
	  ostream.push(0, 1);
	}
	else
	{
	  assert(false);
	}

	if (DEBUG) cerr << hex << "mid dec : " << lower << " < " << code << " < " << (lower + range) << dec << endl;
	
	while (range < HALF)
	{
	  if (lower >= HALF)
	  {
		lower = (lower - HALF) << 1;
		range <<= 1;
		code = (code << 1) & MASK_32;
		code += istream.get(1);
	  }
	  else if(lower + range < HALF)
	  {
		lower <<= 1;
		range <<= 1;
		code = (code << 1) & MASK_32;
		code += istream.get(1);
	  }
	  // else if(QUAT <= lower && lower + range <= THIRD_QUAT)
	  // {
	  // 	lower = (lower - QUAT) << 1;
	  // 	range <<= 1;
	  // 	code -= QUAT;
	  // 	code = (code << 1) & MASK_32;
	  // 	code += istream.get(1);
	  // }
	  else
	  {
		break;
	  }
	}
	if (DEBUG)
	{
	  cerr << endl;
	  cerr << hex << "dec : " << lower << " < " << code << " < " << (lower + range) << dec << endl;
	}
  }
}


// == Image ==

struct Image
{
public:
  uint16_t get(int y, int x);

  void read(ifstream &fin);
  void write(ofstream &fout);

  void enlarge(const int rate, Image &dst);
  void shrink(const int rate, Image &dst);
  
  Image(const int height, const int width);
  ~Image();

  const int64_t height;
  const int64_t width;
  const int64_t size;
  uint16_t *data;
};

// {{{

void Image::enlarge(const int rate, Image &dst)
{
  assert(width * rate == dst.width);
  assert(height * rate == dst.height);
}

void Image::shrink(const int rate, Image &dst)
{
  assert(dst.width * rate == width);
  assert(dst.height * rate == height);
}

Image::Image(const int height, const int width)
  : height(height)
  , width(width)
  , size(height * width)
{
  data = new uint16_t[size];
}

Image::~Image()
{
  delete *data;
}

void Image::read(ifstream &fin)
{
  char *ptr = reinterpret_cast<char *>(data.data());
  fin.read(ptr, size * sizeof(uint16_t));
}

void Image::write(ofstream &fout)
{
  char *ptr = reinterpret_cast<char *>(data.data());
  fout.write(ptr, size * sizeof(uint16_t));
}

// }}}

// == main encoder ==

int64_t predict(const Image &img, const int y, const int x)
{
  int64_t ret = 0;
  int64_t count = 0;
  if (y > 0)
  {
	ret += img.data[y + x - img.width];
	count++;
  }
  if (x > 0)
  {
	ret += img.data[y + x - 1];
	count++;
  }
  if (count > 0)
  {
	return ret / count;
  }
  else
  {
	return (1 << 15);
  }
}

void encode_image(const Image &img, BitStream &ostream)
{
  BypassEncoder enc(&ostream);

  bool prev_unary = true;
  
  for (int y = 0; y < img.size; y += img.width)
  {
	for (int x = 0; x < img.width; x++)
	{
	  const int64_t pred = predict(img, y, x);
	  const int64_t diff = img.data[y + x] - pred;
	  const bool sign = (diff >= 0);
	  
	  enc.encode_unary(sign != prev_unary);
	  prev_unary = sign;
	  
	  // enc.encode_unary(sign);
	  enc.encode_pixel(abs(diff));
	}
  }
}

pair<int, int> get_image_size(const int size)
{
  switch(size)
  {
  case 3000*5000*2:
	return make_pair(3000, 5000);
  case 1500*2500*2:
	return make_pair(1500, 2500);
  case 6000*10000*2:
	return make_pair(6000, 10000);
  default:
	assert(false);
  }
}

const size_t MAX_FILENAME_LENGTH = 100;

template<typename T>
void encode_bypass(ofstream &fout, const T val)
{
  T v = val;
  char *ptr = reinterpret_cast<char *>(&v);
  fout.write(ptr, sizeof(T));
}

void encode_string(ofstream &fout, const string &str, const size_t size)
{
  for (int i = 0; i < str.length(); i++)
  {
	fout.write(&str[i], 1);
  }
  
  const char ch = 0;
  for (int i = 0; i < size - str.length(); i++)
  {
	fout.write(&ch, 1);
  }
}

void count_zero_rate(const BitStream &ostream)
{
  int64_t ans = 0;
  for (uint64_t v : ostream.data_array)
  {
	ans += __builtin_popcountll(v);
  }
  cerr << ans << " / " << (ostream.size_u64() * 64) << " : rate = " << (ans * 1.0 / (ostream.size_u64() * 64)) << endl;
}

void solve_encode(const string output_file_path,
				  const string input_dir_path,
				  vector<pair<string, int>> target_file)
{
  ofstream fout(output_file_path, ios::out | ios::binary);
  assert(fout);

  encode_bypass(fout, target_file.size());
  cerr << "enc: # of pic: " << target_file.size() << endl;

  Image *prev_img = nullptr;
  
  for (auto &v : target_file)
  {
	const string filename = v.first;
	encode_string(fout, filename, MAX_FILENAME_LENGTH);
	cerr << "enc: filename = " << filename << endl;
	
	int64_t height, width;
	tie(height, width) = get_image_size(v.second);
	encode_bypass(fout, height);
	encode_bypass(fout, width);
	
	cerr << "enc: height = " <<  height << ", width = " << width << endl;

	Image *img = new Image(height, width);

	cerr << (input_dir_path + "/" + filename) << endl;
	ifstream fin(input_dir_path + "/" + filename, ios::in | ios::binary);
	img.read(fin);
	fin.close();
	
	BitStream ostream_raw;
	encode_image(img, ostream_raw);
	ostream_raw.trim();

	count_zero_rate(ostream_raw);
	cerr << "enc: binsize = " << ostream_raw.size_u64() << endl;
	
	RangeCoder rc;
	BitStream ostream_rc;
	rc.encode(ostream_raw, ostream_rc);
	ostream_rc.trim();

	cerr << ostream_raw.size() << " -> " << ostream_rc.size() << endl;
	
	encode_bypass(fout, ostream_raw.size_u64());
	ostream_rc.write(fout);
	fout.flush();

	if (prev_img != null)
	{
	  delete prev_img;
	}
	prev_img = img;
  }
  fout.close();
}

// == main decoder ==

template<typename T>
T decode_bypass(ifstream &fin)
{
  T val;
  char *ptr = reinterpret_cast<char *>(&val);
  fin.read(ptr, sizeof(T));
  return val;
}

string decode_string(ifstream &fin, const size_t size)
{
  char *buf = new char[size + 1];
  fill(buf, buf + size + 1, 0);

  fin.read(buf, MAX_FILENAME_LENGTH);
  
  string ret(buf);

  delete[] buf;
  return ret;
}

void decode_image(Image &img, BitStream &istream)
{
  BypassDecoder dec(&istream);

  bool prev_unary = true;
  
  for (int y = 0; y < img.size; y += img.width)
  {
	for (int x = 0; x < img.width; x++)
	{
	  const bool sign = (prev_unary != dec.decode_unary());
	  int64_t diff = dec.decode_pixel();
	  
	  if (!sign)
	  {
		diff = -diff;
	  }
	  
	  diff += predict(img, y, x);
	  img.data[y + x] = diff;

	  prev_unary = sign;
	}
  }
}

void solve_decode(const string input_file_path,
				  const string output_dir_path)
{
  ifstream fin(input_file_path, ios::in | ios::binary);
  assert(fin);
  cerr << "dec: path = " << input_file_path << endl;
  
  size_t size = decode_bypass<size_t>(fin);
  cerr << "dec: # of pic: " << size << endl;
  
  for (int i = 0; i < size; i++)
  {
	const string filename = decode_string(fin, MAX_FILENAME_LENGTH);
	cerr << filename << endl;
	
	int64_t height = decode_bypass<int64_t>(fin);
	int64_t width = decode_bypass<int64_t>(fin);
	cerr << "dec: height, width = " << height << ", " << width << endl;
	Image img(height, width);
	
	const int stream_rc_size = decode_bypass<uint64_t>(fin);
	cerr << "dec: image size = " << stream_rc_size << endl;
	
	BitStream istream_rc;
	istream_rc.read(fin, stream_rc_size);

	RangeCoder rc;
	BitStream istream_raw;
	rc.decode(istream_rc, istream_raw);
	
	decode_image(img, istream_raw);
	
	ofstream fout(output_dir_path + "/" + filename, ios::out | ios::binary);
	assert(fout);
	img.write(fout);
	fout.close();
  }
  fin.close();
}

// == test code ==

// {{{

void test_BitStream()
{
  const int size = 36;
  const int bit_width = 7;
  
  BitStream sout;
  for (uint64_t i = 0; i < size; i++)
  {
	cerr << i << " ";
	sout.push(i, bit_width);
  }
  cerr << endl;
  ofstream fout("result.txt", ios::out | ios::binary);
  sout.write(fout);
  fout.close();

  cerr << "====================" << endl;
  
  BitStream sin;
  ifstream fin("result.txt", ios::in | ios::binary);
  sin.read(fin, (size * bit_width + 7) / 8);
  
  for (uint64_t i = 0; i < size; i++)
  {
	const uint64_t data = sin.get(bit_width);
	cerr << data << " ";
  }
  cerr << endl;
}

void test_encdec_string()
{
  ofstream fout("test2.txt");
  
  string str_enc = "hoge";
  encode_string(fout, str_enc, MAX_FILENAME_LENGTH);
  fout.close();
  
  ifstream fin("test2.txt");

  string str_dec = decode_string(fin, MAX_FILENAME_LENGTH);
  cerr << str_dec << endl;  
  cerr << str_dec.length() << endl;
  
  assert(str_enc == str_dec);
}

void test_rangecoder()
{
  cerr << "test RangeCoder" << endl;

  // == init ==
  
  BitStream ostream_raw;
  for (int i = 0; i < 64; i++)
  {
	ostream_raw.push(1, 1);
	cerr << 1 << " ";
	ostream_raw.push(1, 1);
	cerr << 1 << " ";
	ostream_raw.push(1, 1);
	cerr << 1 << " ";
	ostream_raw.push(0, 1);
	cerr << 0 << " ";
  }
  cerr << endl;
  ostream_raw.trim();

  // == encode ==
  
  BitStream ostream_rc;
  RangeCoder rc;
  rc.encode(ostream_raw, ostream_rc);
  ostream_rc.trim();
  cerr << ostream_raw.size() << " -> " << ostream_rc.size() << endl;
  
  ofstream fout("result.txt", ios::out | ios::binary);
  encode_bypass(fout, ostream_rc.size());
  ostream_rc.write(fout);
  fout.close();

  // == decode ==
   
  ifstream fin("result.txt", ios::in | ios::binary);
  uint64_t size_stream = decode_bypass<uint64_t>(fin);
  BitStream istream_rc;
  istream_rc.read(fin, size_stream);
  fin.close();
  
  BitStream istream_raw;
  rc.decode(istream_rc, istream_raw);

  for (int i = 0; i < 192; i++)
  {
	cerr << istream_raw.get(1) << " ";
  }
  cerr << endl;
}

void test()
{
  test_encdec_string();
  test_BitStream();
  test_rangecoder();
}


// }}}


// == main ==
int main()
{
  // test();
  // return 0;
  
  string order;
  getline(cin, order);

  if ("encode" == order)
  {
	string output_file_path;
	getline(cin, output_file_path);
	string input_dir_path;
	getline(cin, input_dir_path);

	int n;
	cin >> n;
	cin.ignore();

	vector<pair<string, int>> target_file;
	for (int i = 0; i < n; i++)
	{
	  string line;
	  getline(cin, line);
	  stringstream ss(line);

	  string filename;
	  ss >> filename;
	  int filesize;
	  ss >> filesize;
	  target_file.push_back(make_pair(filename, filesize));
	}
	solve_encode(output_file_path, input_dir_path, target_file);
  }
  else // decode
  {
	string input_file_path;
	getline(cin, input_file_path);
	string output_dir_path;
	getline(cin, output_dir_path);
	
	solve_decode(input_file_path, output_dir_path);
  }
  return 0;
}
