#!/bin/bash

rm enc.bin

echo "start encoding"
./a.out < encode.in

rm -r output
mkdir output

echo "start decoding"
./a.out < decode.in

diff bin_4obs output
